package az.spring.service;


import az.spring.entity.User;
import az.spring.model.UserDTO;
import az.spring.repository.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User registerUser(UserDTO userDTO) {
        User user = new User();
        user.setEmail(userDTO.getEmail());
        user.setFirstname(userDTO.getFirstname());
        user.setLastname(userDTO.getLastname());
        user.setRole("USER");
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        //BeanUtils.copyProperties(userDTO, user);
        userRepository.save(user);
        return user;
    }
}
