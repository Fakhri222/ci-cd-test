package az.spring.service;

import az.spring.entity.User;
import az.spring.model.UserDTO;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    User registerUser(UserDTO userDTO);
}
