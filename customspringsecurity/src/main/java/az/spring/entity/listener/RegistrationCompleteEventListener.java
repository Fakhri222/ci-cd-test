package az.spring.entity.listener;

import az.spring.entity.User;
import az.spring.event.RegistrationCompleteEvent;
import org.springframework.context.ApplicationListener;

import java.util.UUID;

public class RegistrationCompleteEventListener implements ApplicationListener<RegistrationCompleteEvent> {

    @Override
    public void onApplicationEvent(RegistrationCompleteEvent event) {
        User us=event.getUser();
        String token= UUID.randomUUID().toString();
    }
}
