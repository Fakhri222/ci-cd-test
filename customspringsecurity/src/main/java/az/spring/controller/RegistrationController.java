package az.spring.controller;

import az.spring.entity.User;
import az.spring.event.RegistrationCompleteEvent;
import az.spring.model.UserDTO;
import az.spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.awt.desktop.AppForegroundEvent;

@RestController
public class RegistrationController {

    @Autowired
    private  UserService userService;

    @Autowired
    private ApplicationEventPublisher publisher;

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public String registerUser(@RequestBody UserDTO userDTO){
        User user=userService.registerUser(userDTO);
        publisher.publishEvent(new RegistrationCompleteEvent(user,"url"));
        return "success";
    }
}
